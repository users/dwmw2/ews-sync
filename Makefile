
ifdef RPM_OPT_FLAGS
OPT_FLAGS := $(RPM_OPT_FLAGS)
else
OPT_FLAGS := -O2 -g -Wall
endif

ifeq ($(PKG_CONFIG),)
PKG_CONFIG=pkg-config
endif

ifeq ($(XML2_CONFIG),)
XML2_CONFIG=xml2-config
endif

XML2_CFLAGS += $(shell $(XML2_CONFIG) --cflags)
XML2_LDFLAGS += $(shell $(XML2_CONFIG) --libs)
ifeq ($(XML2_LDFLAGS),)
$(error "No libxml2 support. Cannot continue");
endif

SOUP_CFLAGS += $(shell $(PKG_CONFIG) libsoup-2.4 --cflags) 
SOUP_LDFLAGS += $(shell $(PKG_CONFIG) libsoup-2.4 --libs)
ifeq ($(SOUP_LDFLAGS),)
$(error "No libsoup support. Cannot continue");
endif

ICAL_CFLAGS += $(shell $(PKG_CONFIG) libical --cflags)
ICAL_LDFLAGS += $(shell $(PKG_CONFIG) libical --libs)
ifeq ($(ICAL_LDFLAGS),)
$(error "No libical support. Cannot continue");
endif

GLIB_CFLAGS += $(shell $(PKG_CONFIG) glib-2.0 --cflags)
GLIB_LDFLAGS += $(shell $(PKG_CONFIG) glib-2.0 --libs)
ifeq ($(GLIB_LDFLAGS),)
$(error "No glib support. Cannot continue");
endif


CFLAGS := $(OPT_FLAGS) $(XML2_CFLAGS) $(GLIB_CFLAGS)
CFLAGS_ews_syncfolder.o := $(SOUP_CFLAGS)
CFLAGS_ews_autodiscover.o := $(SOUP_CFLAGS)
CFLAGS_ews2ical.o := $(ICAL_CFLAGS)
CFLAGS_calitem_to_ical.o := $(ICAL_CFLAGS)
CFLAGS_soup-soap-message.o := $(SOUP_CFLAGS)
CFLAGS_soup-soap-response.o := $(SOUP_CFLAGS)

%.o: %.c
	$(CC) -c -o $@ $(CFLAGS) $(CFLAGS_$@) $< -MD -MF .$@.dep

PROGS := ews_syncfolder ews_autodiscover ews2ical

all: $(PROGS)

-include $(wildcard .*o.dep) /dev/null

clean:
	rm -f $(PROGS) *.o .*.o.dep

ews2ical: ews2ical.o calitem_to_ical.o
	$(CC) -o $@ $^ $(LDFLAGS) $(XML2_LDFLAGS) $(ICAL_LDFLAGS) $(GLIB_LDFLAGS)

ews_autodiscover: ews_autodiscover.o
	$(CC) -o $@ $^ $(LDFLAGS) $(XML2_LDFLAGS) $(SOUP_LDFLAGS) $(GLIB_LDFLAGS)

ews_syncfolder: ews_syncfolder.o calitem_to_ical.o soup-soap-message.o soup-soap-response.o
	$(CC) -o $@ $^ $(LDFLAGS) $(XML2_LDFLAGS) $(SOUP_LDFLAGS) $(GLIB_LDFLAGS) $(ICAL_LDFLAGS)
