#include <libsoup/soup.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libical/icalcomponent.h>

#include "libews.h"

#include "soup-soap-message.h"
#include "soup-soap-response.h"

#define ITEM_CREATE 1
#define ITEM_DELETE 2
#define ITEM_UPDATE 3

struct item_change {
	struct item_change *next;
	int type;
	char itemid[0];
};

void combine_ical_files(void);

gboolean process_changes(SoupSoapResponse *resp, struct item_change **changes,
			 gboolean *last, gchar **syncstate, GError **error);

icalcomponent *fetch_xml_item(SoupSession *sess, char *url, const char *itemid,
			      const char *xml_filename, const char *parent_id,
			      icaltimezone *parent_zone, GError **error);

struct ews_auth {
	const char *username;
	const char *password;
};

static void souptest_authenticate(SoupSession *sess, SoupMessage *msg,
				  SoupAuth *auth, gboolean retrying, gpointer data)
{
	struct ews_auth *authdata = data;
	soup_auth_authenticate (auth, authdata->username, authdata->password);
}

static gchar *get_child_string(xmlNode *node, const gchar *name)
{
	node = soup_soap_parameter_get_first_child_by_name(node, name);
	if (!node)
		return NULL;
	return soup_soap_parameter_get_string_value(node);
}

static SoupSoapMessage *
soup_ews_message_new(const gchar *url, const gchar *request)
{
	SoupSoapMessage *msg = soup_soap_message_new("POST", url, TRUE, NULL, NULL, NULL);

	soup_message_headers_append (SOUP_MESSAGE(msg)->request_headers,
				     "User-Agent", "libews/0.1");

	soup_soap_message_start_envelope(msg);
	soup_soap_message_start_body(msg);
	soup_soap_message_add_namespace(msg, "types", 
				       "http://schemas.microsoft.com/exchange/services/2006/types");
	soup_soap_message_start_element(msg, request, NULL, NULL);
	soup_soap_message_set_default_namespace(msg, "http://schemas.microsoft.com/exchange/services/2006/messages");
	return msg;
}

int main(int argc, char **argv)
{
	GError *error = NULL;
	SoupSession *sess;
	SoupSoapMessage *msg;
	SoupSoapResponse *resp;
	guint status;
	struct ews_auth auth;
	char *url;
	struct item_change *changes = NULL;
	gchar *syncstate = NULL;
	gboolean last_in_range = TRUE;
	char *statefilename;
	gsize length;

	if (argc != 4) {
	usage:
		fprintf(stderr, "Usage: ews_syncfolder <ews_url> <DOMAIN\\username> <password>\n");
		fprintf(stderr, "Sorry, no Kerberos auth support yet (https://bugzilla.gnome.org/587145)\n");
		exit (1);
	}
	auth.username = argv[2];
	auth.password = argv[3];

	if (!strchr(argv[2], '\\')) {
		fprintf(stderr, "No domain in username (https://bugzilla.gnome.org/624613)\n");
		goto usage;
	}
	
        g_thread_init (NULL);
        g_type_init ();

	url = argv[1];

	statefilename = g_build_filename (g_get_home_dir() , ".ews-syncstate", NULL);
	g_file_get_contents(statefilename, &syncstate, &length, NULL);
	
	sess = soup_session_sync_new_with_options(SOUP_SESSION_USE_NTLM, TRUE, NULL);
	g_signal_connect (sess, "authenticate",
			  G_CALLBACK(souptest_authenticate), &auth);

	if (getenv("EWS_DEBUG")) {
		SoupLogger *logger;
		logger = soup_logger_new(SOUP_LOGGER_LOG_BODY, -1);
		soup_session_add_feature(sess, SOUP_SESSION_FEATURE(logger));
	}
 again:
	msg = soup_ews_message_new(url, "SyncFolderItems");

	soup_soap_message_start_element(msg, "ItemShape", NULL, NULL);

	soup_soap_message_start_element(msg, "BaseShape", "types", NULL);
	soup_soap_message_write_string(msg, "Default");
	soup_soap_message_end_element(msg);

	soup_soap_message_end_element(msg); /* ItemShape */

	soup_soap_message_start_element(msg, "SyncFolderId", NULL, NULL);

	soup_soap_message_start_element(msg, "DistinguishedFolderId", "types", NULL);
	soup_soap_message_add_attribute(msg, "Id", "calendar", NULL, NULL);
	soup_soap_message_end_element(msg);

	soup_soap_message_end_element(msg); /* SyncFolderId */

	if (syncstate) {
		if (syncstate[0]) {
			soup_soap_message_start_element(msg, "SyncState", NULL, NULL);
			soup_soap_message_write_string(msg, syncstate);
			soup_soap_message_end_element(msg);
		}
		g_free(syncstate);
	}
	soup_soap_message_start_element(msg, "MaxChangesReturned", NULL, NULL);
	soup_soap_message_write_int(msg, 5);
	soup_soap_message_end_element(msg);

	soup_soap_message_end_element(msg); /* SyncFolderItems */
	soup_soap_message_end_body(msg);
	soup_soap_message_end_envelope(msg);

	soup_soap_message_persist(msg);

	status = soup_session_send_message(sess, SOUP_MESSAGE(msg));

	if (!SOUP_STATUS_IS_SUCCESSFUL(status)) {
		const gchar *error = soup_status_get_phrase(status);
		if (!error)
			error = "Unknown error";

		fprintf(stderr, "SyncFolderItems message failed: %s\n", error);
		exit(1);
	}

	resp = soup_soap_message_parse_response(msg);
	if (!resp) {
		fprintf(stderr, "Failed to parse SyncFolderItems response\n");
		exit(1);
	}
	if (!process_changes(resp, &changes, &last_in_range, &syncstate, &error)) {
		fprintf(stderr, "Process changes failed: %s\n", error->message);
		exit(1);
	}

	g_object_unref(resp);

	while (changes) {
		struct item_change *this = changes;
		char *xml_filename = g_strdup_printf("%s/ews-sync/%s.xml",
						     g_get_home_dir(), this->itemid);
		char *ics_filename = g_strdup_printf("%s/ews-sync/%s.ics",
						     g_get_home_dir(), this->itemid);
		if (this->type == ITEM_DELETE) {
			printf("Deleting %s\n", xml_filename);
			unlink(xml_filename);
			unlink(ics_filename);
		} else {
			GError *error = NULL;
			icalcomponent *comp;

			comp = fetch_xml_item(sess, url, this->itemid,
					      xml_filename, NULL, NULL, &error);
			if (!comp) {
				fprintf(stderr, "Fetch error: %s\n", error->message);
				g_clear_error(&error);
			} else {
				char *outbuf;
				outbuf = icalcomponent_as_ical_string_r(comp);

				g_file_set_contents(ics_filename, outbuf, strlen(outbuf), NULL);
				printf("Got ICS file %s\n", ics_filename);
				free(outbuf);
				icalcomponent_free(comp);
			}
		}
		changes = this->next;
		free(this);
	}

	g_file_set_contents(statefilename, syncstate, strlen(syncstate), NULL);
	if (!last_in_range) {
		printf("Not last in range; restarting\n");
		goto again;
	}
	g_free(syncstate);
	g_free(statefilename);

	combine_ical_files();
	return 0;
}

struct _state {
	SoupSession *sess;
	char *url;
};

icalcomponent *fetch_xml_subitem(void *_st, const char *itemid,
				 const char *parent_id, icaltimezone *parent_zone, GError **error)
{
	struct _state *st = _st;
	return fetch_xml_item(st->sess, st->url, itemid, NULL, parent_id, parent_zone, error);
}

icalcomponent *fetch_xml_item(SoupSession *sess, char *url, const char *itemid,
			      const char *xml_filename, const char *parent_id,
			      icaltimezone *parent_zone, GError **error)
{
	struct _state st = {
		.sess = sess,
		.url = url,
	};
	SoupSoapMessage *msg;
	SoupSoapResponse *resp;
	xmlNode *node;
	int status;
	char *responseclass;
	icalcomponent *calcomp = NULL;

	msg = soup_ews_message_new(url, "GetItem");

	soup_soap_message_start_element(msg, "ItemShape", NULL, NULL);
		
	soup_soap_message_start_element(msg, "BaseShape", "types", NULL);
	soup_soap_message_write_string(msg, "AllProperties");
	soup_soap_message_end_element(msg);

	soup_soap_message_start_element(msg, "BodyType", "types", NULL);
	soup_soap_message_write_string(msg, "Text");
	soup_soap_message_end_element(msg);

	soup_soap_message_end_element(msg); /* ItemShape */

	soup_soap_message_start_element(msg, "ItemIds", NULL, NULL);
	soup_soap_message_start_element(msg, "ItemId", "types", NULL);
	soup_soap_message_add_attribute(msg, "Id", itemid, NULL, NULL);
	soup_soap_message_end_element(msg);
	soup_soap_message_end_element(msg);

	soup_soap_message_end_element(msg); /* SyncFolderItems */
	soup_soap_message_end_body(msg);
	soup_soap_message_end_envelope(msg);

	soup_soap_message_persist(msg);

	status = soup_session_send_message(sess, SOUP_MESSAGE(msg));

	if (!SOUP_STATUS_IS_SUCCESSFUL(status)) {
		const gchar *errtxt = soup_status_get_phrase(status);
		if (!errtxt)
			errtxt = "Unknown error";

		g_set_error(error, EWS_ERROR, EWS_ERROR_SOAP,
			    "GetItem message failed: %s", errtxt);
		return NULL;
	}

	if (xml_filename) {
		g_file_set_contents(xml_filename, SOUP_MESSAGE(msg)->response_body->data,
				    SOUP_MESSAGE(msg)->response_body->length, NULL);

		printf("Got XML file %s\n", xml_filename);
	}

	resp = soup_soap_message_parse_response(msg);
	g_object_unref(msg);
	if (!resp) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to parse GetItem response");
		return NULL;
	}

	node = soup_soap_response_get_first_parameter_by_name(resp, "ResponseMessages");
	if (!node) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No <ResponseMessages> element in GetItems reply");
		goto out;
	}

	node = soup_soap_parameter_get_first_child_by_name(node, "GetItemResponseMessage");
	if (!node) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No <GetItemResponseMessage> element in GetItem reply");
		goto out;
	}
	
	responseclass = soup_soap_parameter_get_property(node, "ResponseClass");
	if (!strcmp(responseclass, "Error")) {
		gchar *msgtext = NULL;
		gchar *respcode = NULL;
		xmlNode *n2;

		n2 = soup_soap_parameter_get_first_child_by_name(node, "MessageText");
		if (n2)
			msgtext = soup_soap_parameter_get_string_value(n2);

		n2 = soup_soap_parameter_get_first_child_by_name(node, "ResponseCode");
		if (n2)
			respcode = soup_soap_parameter_get_string_value(n2);

		g_set_error(error, EWS_ERROR, EWS_ERROR_SERVER,
			    "Server returned error: %s (ResponseCode %s)",
			    msgtext, respcode);
		g_free(responseclass);
		g_free(msgtext);
		g_free(respcode);
		goto out;
	} else if (strcmp(responseclass, "Success")) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Unknown response class '%s' from server",
			    responseclass);
		g_free(responseclass);
		goto out;
	}

	node = soup_soap_parameter_get_first_child_by_name(node, "Items");
	if (!node) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No <Items> element in GetItem reply");
		goto out;
	}

	node = soup_soap_parameter_get_first_child_by_name(node, "CalendarItem");
	if (!node) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to find <CalendarItem> element in GetItem reply");
		goto out;
	}

	calcomp = ews_calitem_to_ical(node, parent_id, parent_zone, fetch_xml_subitem, (void *)&st, error);
 out:
	g_object_unref(resp);

	return calcomp;
}

gboolean process_changes(SoupSoapResponse *resp, struct item_change **changes,
			 gboolean *last, gchar **syncstate, GError **error)
{
	xmlNode *node, *node2;
	char *str;

	node = soup_soap_response_get_first_parameter_by_name(resp, "ResponseMessages");
	if (!node) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No <ResponseMessages> element in SyncFolderItems reply");
		return FALSE;
	}
	node = soup_soap_parameter_get_first_child_by_name(node, "SyncFolderItemsResponseMessage");
	if (!node) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No <SyncFolderItemsResponseMessage> element in SyncFolderItems reply");
		return FALSE;
	}
	str = soup_soap_parameter_get_property(node, "ResponseClass");
	if (!str) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No ResponseClass parameter in SyncFolderItems reply");
		return FALSE;
	}
	if (!strcmp(str, "Error")) {
		gchar *msgtext = NULL;
		gchar *respcode = NULL;
		xmlNode *n2;

		n2 = soup_soap_parameter_get_first_child_by_name(node, "MessageText");
		if (n2)
			msgtext = soup_soap_parameter_get_string_value(n2);

		n2 = soup_soap_parameter_get_first_child_by_name(node, "ResponseCode");
		if (n2)
			respcode = soup_soap_parameter_get_string_value(n2);

		g_set_error(error, EWS_ERROR, EWS_ERROR_SERVER,
			    "Server returned error: %s (ResponseCode %s)",
			    msgtext, respcode);
		g_free(msgtext);
		g_free(respcode);
		g_free(str);
		return FALSE;
	} else if (strcmp(str, "Success")) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Unknown response class '%s' from server",
			    str);
		g_free(str);
		return FALSE;
	}
	g_free(str);
	str = get_child_string(node, "IncludesLastItemInRange");
	if (!str) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No LastItemInRange element in SyncFolderItems response");
		return FALSE;
	}
	if (!strcmp(str, "true"))
		*last = TRUE;
	else if (!strcmp(str, "false"))
		*last = FALSE;
	else {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Invalid value for <IncludesLastItemInRange>: %s",
			    str);
		g_free(str);
		return FALSE;
	}
	g_free(str);

	*syncstate = (gchar *)get_child_string(node, "SyncState");
	if (!*syncstate) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No SyncState element in SyncFolderItems response");
		return FALSE;
	}
	node = soup_soap_parameter_get_first_child_by_name(node, "Changes");
	if (!node)
		return TRUE;

	for (node = soup_soap_parameter_get_first_child(node);
	     node; node = soup_soap_parameter_get_next_child(node)) {
		struct item_change *new_change;
		char *itemid;
		int type;

		if (!strcmp((char *)node->name, "Create")) {
			type = ITEM_CREATE;
		} else if (!strcmp((char *)node->name, "Update")) {
			type = ITEM_UPDATE;
		} else if (!strcmp((char *)node->name, "Delete")) {
			type = ITEM_DELETE;
			node2 = node;
			goto itemid;
		} else {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "Unknown change type '%s'", 
				    node->name);
		err_changes:
			while (*changes) {
				new_change = *changes;
				*changes = new_change->next;
				g_free(new_change);
			}
			return FALSE;
		}
		node2 = soup_soap_parameter_get_first_child_by_name(node, "CalendarItem");
		if (!node2) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "<%s> node has no <CalendarItem> child",
				    node->name);
			goto err_changes;
		}
	itemid:
		node2 = soup_soap_parameter_get_first_child_by_name(node2, "ItemId");
		if (!node2) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "<%s> node has no <ItemId> child",
				    node->name);
			goto err_changes;
		}
		itemid = (char *)xmlGetProp(node2, (xmlChar *)"Id");

		new_change = malloc(sizeof(*new_change) + strlen(itemid) + 1);
		if (!new_change) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "Out of memory");
			goto err_changes;
		}
		new_change->next = *changes;
		new_change->type = type;
		strcpy(new_change->itemid, itemid);
		*changes = new_change;
	}
	return TRUE;
}

void combine_ical_files(void)
{
	icalcomponent *merged, *this;
	const gchar *name;
	gchar *dirname;
	gchar *filename;
	gchar *contents;
	GDir *d;

	merged = icalcomponent_new_vcalendar();

	dirname = g_build_filename (g_get_home_dir() , "ews-sync", NULL);

	d = g_dir_open (dirname, 0, NULL);
	if (!d)
		return;

	while ((name = g_dir_read_name(d))) {
		if (!strcmp(name, "all.ics"))
			continue;
		if (strlen(name) < 4)
			continue;
		if (strcmp(name + strlen(name) - 4, ".ics"))
			continue;

		filename = g_build_filename(dirname, name, NULL);
		if (!g_file_get_contents(filename, &contents, NULL, NULL))
			continue;

		this = icalcomponent_new_from_string(contents);
		if (!this)
			continue;
		
		icalcomponent_merge_component(merged, this);
		g_free(contents);
	}
	filename = g_build_filename(dirname, "all.ics", NULL);
	contents = icalcomponent_as_ical_string_r(merged);

	g_file_set_contents(filename, contents, strlen(contents), NULL);
	printf("Write all.ics file\n");
	g_free(contents);
	icalcomponent_free(merged);
}
