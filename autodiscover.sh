#!/bin/sh

EMAIL="$1"
DOMAIN=${EMAIL##*@}
CURLAUTH="--negotiate -u dummy:"
# NTLM is faster, but requires your password

if [ "$3" != "" ]; then
    CURLAUTH="--ntlm -u $2:$3"
fi
CURLAUTH="-k --connect-timeout 5 $CURLAUTH"

QUERYFILE=$(mktemp /tmp/ewsqueryXXXXXX)
RESULTFILE=$(mktemp /tmp/ewsresultXXXXXX)
trap 'rm $QUERYFILE $RESULTFILE' EXIT

cat > $QUERYFILE <<EOF
<Autodiscover xmlns='http://schemas.microsoft.com/exchange/autodiscover/outlook/requestschema/2006'>
  <Request>
    <EMailAddress>$EMAIL</EMailAddress>
    <AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a</AcceptableResponseSchema>
  </Request>
</Autodiscover>
EOF

for ATTEMPT in $DOMAIN autodiscover.$DOMAIN; do
    if [ -n $EWSURL ]; then
	echo Trying $ATTEMPT...
	if curl $CURLAUTH -L -H "Content-Type: text/xml" \
	    https://$ATTEMPT/autodiscover/autodiscover.xml \
	    -d @$QUERYFILE > $RESULTFILE; then
	    EWSURL="$(sed -n -e '/EwsUrl/{s/.*<EwsUrl>\(.*\)<\/EwsUrl>.*/\1/p;q}' $RESULTFILE)"
	    if [ "$EWSURL" != "" ]; then
		echo "Found EWS URL: $EWSURL"
		exit 0
	    fi
	fi
    fi
done

echo "Failed to discover EWS URL"
exit 1
