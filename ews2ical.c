#include <libxml/parser.h>
#include <libxml/tree.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <libical/icaltime.h>
#include <libical/icalduration.h>
#include <libical/icaltimezone.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gerror.h>

#include "libews.h"

icalcomponent *fetch_item(int xmlfd, const char *parent_id, icaltimezone *zone, GError **error);

icalcomponent *(hack_fetch_subitem)(void *priv,
				    const gchar *item_id,
				    const gchar *parent_id,
				    icaltimezone *zone,
				    GError **error);

int main(int argc, char **argv)
{
	FILE *calfile;
	GError *error = NULL;
	icalcomponent *calcomp;
	int xmlfd = 0; /* stdin */
	char *outbuf;

	if (argc >= 2) {
		xmlfd = open(argv[1], O_RDONLY);
		if (xmlfd < 0) {
			perror("open xml file");
			return -1;
		}
	}
	if (argc >= 3) {
		calfile = fopen(argv[2], "w");
		if (!calfile) {
			perror("open cal file");
			return -1;
		}
	} else
		calfile = fdopen(1, "w");

		
	if (argc >= 4) {
		fprintf(stderr, "Too many args. Want only xml file and ical file\n");
		return -1;
	}
	calcomp = fetch_item(xmlfd, NULL, NULL, &error);

	if (!calcomp) {
		fprintf(stderr, "Failed to convert: %s\n", error->message);
		g_clear_error(&error);
		return 1;
	}
	outbuf =icalcomponent_as_ical_string_r(calcomp);
	fprintf(calfile, "%s", outbuf);
	free(outbuf);
	icalcomponent_free(calcomp);
	return 0;
}

icalcomponent *fetch_item(int xmlfd, const char *parent_id, icaltimezone *zone, GError **error)
{
	xmlDocPtr xml_doc;
	xmlNode *xml_node;
	icalcomponent *comp;

	char buf[1];
	read(xmlfd, buf, 1);
	if (*buf == '<')
		lseek(xmlfd, 0, SEEK_SET);
	xml_doc = xmlReadFd(xmlfd, "noname.xml", "utf-8", XML_PARSE_RECOVER);
	if (!xml_doc) {
		fprintf(stderr, "Failed to parse XML\n");
		exit(1);
	}
	xml_node = xmlDocGetRootElement(xml_doc);
	if (!xml_node) {
		fprintf(stderr, "Failed to parse XML\n");
		exit(1);
	}
	if (xml_node->type != XML_ELEMENT_NODE ||
	    strcmp((char *)xml_node->name, "Envelope")) {
		fprintf(stderr, "Root node not as expected: %s\n", xml_node->name);
		exit(1);
	}
	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type == XML_ELEMENT_NODE &&
		    !strcmp((char *)xml_node->name, "Body"))
			break;
	}
	if (!xml_node) {
		fprintf(stderr, "SOAP Body node not found\n");
		exit(1);
	}

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type == XML_ELEMENT_NODE &&
		    !strcmp((char *)xml_node->name, "GetItemResponse"))
			break;
	}
	if (!xml_node) {
		fprintf(stderr, "GetItemResponse node not found\n");
		exit(1);
	}

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type == XML_ELEMENT_NODE &&
		    !strcmp((char *)xml_node->name, "ResponseMessages"))
			break;
	}
	if (!xml_node) {
		fprintf(stderr, "ResponseMessages node not found\n");
		exit(1);
	}

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type == XML_ELEMENT_NODE &&
		    !strcmp((char *)xml_node->name, "GetItemResponseMessage"))
			break;
	}
	if (!xml_node) {
		fprintf(stderr, "GetItemResponseMessage node not found\n");
		exit(1);
	}

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type == XML_ELEMENT_NODE &&
		    !strcmp((char *)xml_node->name, "Items"))
			break;
	}
	if (!xml_node) {
		fprintf(stderr, "Items node not found\n");
		exit(1);
	}

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type == XML_ELEMENT_NODE &&
		    !strcmp((char *)xml_node->name, "CalendarItem"))
			break;
	}
	if (!xml_node) {
		fprintf(stderr, "CalendarItem node not found\n");
		exit(1);
	}

	comp = ews_calitem_to_ical(xml_node, parent_id, zone, NULL /*hack_fetch_subitem*/, NULL, error);
	xmlFreeDoc(xml_doc);
	return comp;
}


icalcomponent *(hack_fetch_subitem)(void *priv,
				    const gchar *item_id,
				    const gchar *parent_id,
				    icaltimezone *zone,
				    GError **error)
{
	icalcomponent *comp;

	int fd = open(item_id, O_RDONLY);
	if (fd == -1) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to open file '%s'", item_id);
		return NULL;
	}
	comp = fetch_item(fd, parent_id, zone, error);
#if 0
	printf("subitem %p\n", comp);
	if (comp) {
		char *outbuf =icalcomponent_as_ical_string_r(comp);
		fprintf(stderr, "%s", outbuf);
		free(outbuf);
	}
#endif
	return comp;
}
