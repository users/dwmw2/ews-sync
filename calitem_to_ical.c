#include <libxml/parser.h>
#include <libxml/tree.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <libical/icaltime.h>
#include <libical/icalduration.h>
#include <libical/icaltimezone.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gquark.h>

#include "libews.h"

GQuark ews_error_quark(void)
{
	return g_quark_from_static_string("exchange-web-services");
}

gboolean process_relativeyearlyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error);
gboolean process_absoluteyearlyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error);
gboolean process_relativemonthlyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error);
gboolean process_absolutemonthlyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error);
gboolean process_weeklyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error);
gboolean process_dailyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error);

gboolean process_organizer(icalcomponent *comp, xmlNode *xml_node, GError **error);
gboolean process_required_attendees(icalcomponent *comp, xmlNode *xml_node, GError **error);
gboolean process_optional_attendees(icalcomponent *comp, xmlNode *xml_node, GError **error);
gboolean process_time(icalcomponent *comp, xmlNode *xml_node, icaltimetype *ical_time, GError **error);
gboolean process_orig_start(icalcomponent *comp, xmlNode *xml_node, icaltimezone *icaltz, GError **error);
gboolean process_truefalse(icalcomponent *comp, xmlNode *xml_node, gboolean *val, GError **error);
gboolean process_location(icalcomponent *comp, xmlNode *xml_node, GError **error);
gboolean process_sequence(icalcomponent *comp, xmlNode *xml_node, GError **error);
gboolean process_body(icalcomponent *comp, xmlNode *xml_node, GError **error);
gboolean process_subject(icalcomponent *comp, xmlNode *xml_node, GError **error);
gboolean process_recurrence(icalcomponent *comp, xmlNode *xml_node, icaltimezone *zone, GError **error);
gboolean process_deleted_occurrences(icalcomponent *comp, xmlNode *xml_node,
				     icaltimetype **deletia, int *count, GError **error);
gboolean process_modified_occurrences(icalcomponent *comp, xmlNode *xml_node,
				       const char ***modifia, int *count, GError **error);
gboolean process_itemid(icalcomponent *comp, xmlNode *xmlnode, GError **error);
gboolean process_reminder_mins(icalcomponent *comp, xmlNode *xmlnode, GError **error);
icaltimezone *get_timezone(xmlNode *xmlnode, GError **error);
icaltimezone *get_meeting_timezone(xmlNode *xml_node, GError **error);

icalcomponent *ews_calitem_to_ical(xmlNode *xml_node, const gchar *parent_id,
				   icaltimezone *parent_zone, 
				   icalcomponent *(fetch_subitem)(void *priv,
								  const gchar *item_id,
								  const gchar *parent_id,
								  icaltimezone *zone,
								  GError **error),
				   void *fetch_subitem_priv,
				   GError **error)
{
	icaltimetype dtstart, dtend;
	icaltimetype *deletia = NULL;
	const char **modifia = NULL; 
	int i, deletia_count = 0, modifia_count = 0;
	icaltimezone *icaltz;
	icalcomponent *comp, *calcomp;
	icalproperty *prop;
	gboolean allday = FALSE;

	dtstart = dtend = icaltime_null_time();
	

	if (parent_zone) {
		calcomp = NULL;
		icaltz = parent_zone;
	} else {
		calcomp = icalcomponent_new_vcalendar();
		icalcomponent_set_method(calcomp, ICAL_METHOD_PUBLISH);
		prop = icalproperty_new_version("2.0");
		icalcomponent_add_property(calcomp, prop);

		icaltz = get_meeting_timezone(xml_node, error);
		if (!icaltz)
			icaltz = get_timezone(xml_node, error);

		if (icaltz) {
			icalcomponent *comp = icaltimezone_get_component(icaltz);
			icalcomponent_add_component(calcomp, comp);
		}
	}

	comp = icalcomponent_new(ICAL_VEVENT_COMPONENT);

	if (parent_id)
		icalcomponent_set_uid(comp, parent_id);

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "Organizer")) {
			if (!process_organizer(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "RequiredAttendees")) {
			if (!process_required_attendees(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "OptionalAttendees")) {
			if (!process_optional_attendees(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "Start")) {
			if (!process_time(comp, xml_node, &dtstart, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "End")) {
			if (!process_time(comp, xml_node, &dtend, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "Body")) {
			if (!process_body(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "Location")) {
			if (!process_location(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "Subject")) {
			if (!process_subject(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "Recurrence")) {
			if (!process_recurrence(comp, xml_node, icaltz, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "DeletedOccurrences")) {
			if (!process_deleted_occurrences(comp, xml_node, &deletia, &deletia_count, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "ModifiedOccurrences")) {
			if (!process_modified_occurrences(comp, xml_node, &modifia, &modifia_count, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "OriginalStart")) {
			if (!process_orig_start(comp, xml_node, icaltz, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "ItemId")) {
			if (!parent_id && !process_itemid(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "IsAllDayEvent")) {
			if (!process_truefalse(comp, xml_node, &allday, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "ReminderMinutesBeforeStart")) {
			if (!process_reminder_mins(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "AppointmentSequenceNumber")) {
			if (!process_sequence(comp, xml_node, error))
				goto err;
		} else if (!strcmp((char *)xml_node->name, "ParentFolderId") ||
			 !strcmp((char *)xml_node->name, "DateTimeReceived") ||
			 !strcmp((char *)xml_node->name, "Size") ||
			 !strcmp((char *)xml_node->name, "IsSubmitted") ||
			 !strcmp((char *)xml_node->name, "IsDraft") ||
			 !strcmp((char *)xml_node->name, "IsFromMe") ||
			 !strcmp((char *)xml_node->name, "IsResend") ||
			 !strcmp((char *)xml_node->name, "IsUnmodified") ||
			 !strcmp((char *)xml_node->name, "DateTimeSent") ||
			 !strcmp((char *)xml_node->name, "DateTimeCreated") ||
			 !strcmp((char *)xml_node->name, "ResponseObjects") ||
			 !strcmp((char *)xml_node->name, "DisplayCc") ||
			 !strcmp((char *)xml_node->name, "DisplayTo") ||
			 !strcmp((char *)xml_node->name, "Culture") ||
			 !strcmp((char *)xml_node->name, "IsRecurring") ||
			 !strcmp((char *)xml_node->name, "MeetingRequestWasSent") ||
			 !strcmp((char *)xml_node->name, "IsResponseRequested") ||
			 !strcmp((char *)xml_node->name, "MyResponseType") ||
			 !strcmp((char *)xml_node->name, "ConflictingMeetingCount") ||
			 !strcmp((char *)xml_node->name, "AdjacentMeetingCount") ||
			 !strcmp((char *)xml_node->name, "TimeZone") ||
			 !strcmp((char *)xml_node->name, "AppointmentSequenceNumber") ||
			 !strcmp((char *)xml_node->name, "AppointmentState")) {
				 /* Ignore these */
		}
#if 0
	else
		fprintf(stderr, "Unhandled node type '%s'\n", xml_node->name);
#endif
	}

	/* We don't handle really floating events -- which change their time
	   according to the time zone of the observer (like lunch at noon
	   under the sundial wherever you are in the world). But that's OK;
	   Exchange doesn't seem to either:
	   - AFAICT, you can't create them with Outlook.
	   - If you send Exchange an invitation with floating times, which
	     happens to have a VTIMEZONE, it'll assume that timezone (in
	     violation of RFC2445).
	   - If you send Exchange an invitation with floating times with
	     *no* stray VTIMEZONE in the file (which was a mistake), then
	     it creates an item with no timezone but does weird things --
	     this invite:
		DTSTART:20100720T120000
		DTEND:20100720T120010
	     ... leads to an Exchange object saying...
		<t:Start>2010-07-20T11:00:00Z</t:Start>
		<t:End>2010-07-20T11:00:00Z</t:End>

	   For any recurring object without time zones (including the last
	   test above, as well as all day events such as birthdays created
	   with Outlook/Exchange 2003, Exchange will refuse to return the
	   object if the <Recurrence> field is requested, reporting
	   'Corrupt Data'.

	   Tested with Exchange 2007.
	*/
	if (icaltz && !allday) {
		dtstart = icaltime_convert_to_zone(dtstart, icaltz);
		dtend = icaltime_convert_to_zone(dtend, icaltz);
	}
	if (allday) {
		dtstart.is_date = 1;
		dtend.is_date = 1;
	}
	if (!icaltime_is_null_time(dtstart))
		icalcomponent_set_dtstart(comp, dtstart);

	if (!icaltime_is_null_time(dtend))
		icalcomponent_set_dtend(comp, dtend);
	if (deletia) {
		for (i = 0; i < deletia_count; i++) {
			if (icaltz)
				deletia[i] = icaltime_convert_to_zone(deletia[i], icaltz);
			prop = icalproperty_new_exdate(deletia[i]);
			icalcomponent_add_property(comp, prop);
		}
		free(deletia);
		deletia = NULL;
	}
	if (calcomp) {
		icalcomponent_add_component(calcomp, comp);

		if (modifia) {
			const char *uid = icalcomponent_get_uid(comp);
			
			for (i = 0; i < modifia_count; i++) {
				if (fetch_subitem) {
					comp = fetch_subitem(fetch_subitem_priv, modifia[i], uid, icaltz, error);
					if (!comp)
						goto err;
					icalcomponent_add_component(calcomp, comp);
				}
			}
			free(modifia);
		}
	}

	if (icaltz && !parent_zone)
		icaltimezone_free(icaltz, 1);

	return calcomp?:comp;

 err:
	if (comp)
		icalcomponent_free(comp);
	if (calcomp)
		icalcomponent_free(calcomp);
	if (deletia)
		free(deletia);
	if (modifia)
		free(modifia);

	return NULL;
			
}

gboolean process_mailbox(xmlNode *xml_node, const char **r_name, const char **r_email, GError **error)
{
	const char *type = NULL, *name = NULL, *email = NULL;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "Name"))
			name = (char *)xmlNodeGetContent(xml_node);
		if (!strcmp((char *)xml_node->name, "EmailAddress"))
			email = (char *)xmlNodeGetContent(xml_node);
		if (!strcmp((char *)xml_node->name, "RoutingType")) {
			type = (char *)xmlNodeGetContent(xml_node);
		}
	}

	/* We seem to get EX routing for people who don't exist any more */
	if (type && strcmp(type, "SMTP")) {
		g_set_error(error, EWS_ERROR,
			    strcmp(type, "EX")?EWS_ERROR_ROUTING_UNKNOWN:EWS_ERROR_ROUTING_EX,
			    "Unknown RoutingType '%s' for '%s' ('%s')",
			    type, email, name);
		return FALSE;
	}
	*r_name = name;
	*r_email = email;
	return TRUE;
}

gboolean process_organizer(icalcomponent *comp, xmlNode *xml_node, GError **error)
{
	icalproperty *prop;
	icalparameter *param;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "Mailbox")) {
			const char *name = NULL, *email = NULL;
			char *mailtoname;
			if (!process_mailbox(xml_node, &name, &email, error))
				return FALSE;

			mailtoname = g_strdup_printf("mailto:%s", email);
			
			prop = icalproperty_new_organizer(mailtoname);
			g_free(mailtoname);
			param = icalparameter_new_cn(name);
			icalproperty_add_parameter(prop, param);
			icalcomponent_add_property(comp, prop);
			return TRUE;
		}
	}
	g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
		    "No <Mailbox> element in <Organizer>");
	return FALSE;
}

gboolean process_attendee(icalcomponent *comp, xmlNode *xml_node, icalparameter_role role, GError **error)
{
	icalproperty *prop;
	icalparameter *param;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "Mailbox")) {
			GError *local_error = NULL;
			const char *name = NULL, *email = NULL;
			char *mailtoname;
			if (!process_mailbox(xml_node, &name, &email, &local_error)) {
				/* Warn, but don't fail */
				g_warning(local_error->message);
				g_clear_error(&local_error);
				return TRUE;
			}

			mailtoname = g_strdup_printf("mailto:%s", email);
			
			prop = icalproperty_new_attendee(mailtoname);
			g_free(mailtoname);
			param = icalparameter_new_cn(name);
			icalproperty_add_parameter(prop, param);
			param = icalparameter_new_role(role);
			icalproperty_add_parameter(prop, param);
			icalcomponent_add_property(comp, prop);
			return TRUE;
		}
	}
	g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
		    "No <Mailbox> element in <Attendee>");
	return FALSE;
}

gboolean process_required_attendees(icalcomponent *comp, xmlNode *xml_node, GError **error)
{
	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "Attendee")) {
			if (!process_attendee(comp, xml_node,
					      ICAL_ROLE_REQPARTICIPANT, error))
				return FALSE;
		}
	}
	return TRUE;
}

gboolean process_optional_attendees(icalcomponent *comp, xmlNode *xml_node, GError **error)
{
	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "Attendee")) {
			if (!process_attendee(comp, xml_node,
					     ICAL_ROLE_OPTPARTICIPANT, error))
				return FALSE;
		}
	}
	return TRUE;
}

gboolean process_time(icalcomponent *comp, xmlNode *xml_node, icaltimetype *ical_time, GError **error)
{
	char *ews_time = (char *)xmlNodeGetContent(xml_node);

	if (!ews_time) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<%s> node is empty", xml_node->name);
		return FALSE;
	}

	*ical_time = icaltime_from_string(ews_time);
	return TRUE;
}

gboolean process_orig_start(icalcomponent *comp, xmlNode *xml_node, icaltimezone *icaltz, GError **error)
{
	icalproperty *prop;
	const char *tzid = NULL;
	icaltimetype t;

	if (!process_time(comp, xml_node, &t, error))
		return FALSE;

	if (icaltz) {
		t = icaltime_convert_to_zone(t, icaltz);
	}

	/* Grr, icalproperty_add_recurrenceid() doesn't include TZID */
	prop = icalproperty_new_recurrenceid(t);
	tzid = icaltime_get_tzid(t);
	if (tzid)
		icalproperty_add_parameter(prop, icalparameter_new_tzid(tzid));
	icalcomponent_add_property(comp, prop);
	return TRUE;
}

gboolean process_truefalse(icalcomponent *comp, xmlNode *xml_node, gboolean *val, GError **error)
{
	char *truth = (char *)xmlNodeGetContent(xml_node);

	if (!truth) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<%s> node is empty", xml_node->name);
		return FALSE;
	}
	if (!strcmp(truth, "true"))
		*val = TRUE;
	else if (!strcmp(truth, "false"))
		*val = FALSE;
	else {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Unrecognised truth value '%s' in <%s> node",
			    truth, xml_node->name);
		return FALSE;
	}
	return TRUE;
}

gboolean process_location (icalcomponent *comp, xmlNode *xml_node, GError **error)
{
	const char *loc = (char *)xmlNodeGetContent(xml_node);

	if (!loc) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<%s> node is empty", xml_node->name);
		return FALSE;
	}

	icalcomponent_set_location(comp, loc);
	return TRUE;
}

gboolean process_sequence (icalcomponent *comp, xmlNode *xml_node, GError **error)
{
	const char *seq = (char *)xmlNodeGetContent(xml_node);

	if (!seq) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<%s> node is empty", xml_node->name);
		return FALSE;
	}		

	icalcomponent_set_sequence(comp, strtol(seq, NULL, 10));
	return TRUE;
}

gboolean process_body(icalcomponent *comp, xmlNode *xml_node, GError **error)
{
	const char *body = (char *)xmlNodeGetContent(xml_node);

	if (!body) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<%s> node is empty", xml_node->name);
		return FALSE;
	}

	icalcomponent_set_description(comp, body);
	return TRUE;
}

gboolean process_subject(icalcomponent *comp, xmlNode *xml_node, GError **error)
{
	const char *subject = (char *)xmlNodeGetContent(xml_node);

	if (!subject) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<%s> node is empty", xml_node->name);
		return FALSE;
	}

	icalcomponent_set_summary(comp, subject);
	return TRUE;
}


static int month_to_number(const char *month, GError **error)
{
	static char *months[] = {
		"January", "February", "March", "April", "May", "June", "July", 
		"August", "September", "October", "November", "December"
	};
	int monthnr;
	for (monthnr = 0; monthnr < 12; monthnr++) {
		if (!strcmp(month, months[monthnr]))
			return monthnr + 1;
	}

	g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
		    "Unrecognised month name '%s'", month);
	return 0;
}
static int weekday_to_number(const char *day, int accept, GError **error)
{
	static char *days[] = {
		"Sunday", "Monday", "Tuesday", "Wednesday",
		"Thursday", "Friday", "Saturday",
		"Day", "Weekday", "WeekendDay"
	};
	int daynr;
	for (daynr = 0; daynr < accept; daynr++) {
		if (!strcmp(day, days[daynr]))
			return daynr + 1;
	}

	g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
		    "Unrecognised day name '%s'", day);
	return 0;
}

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#endif

static int weekindex_to_ical(const char *week, GError **error)
{
	static struct {
		char *exch;
		int week;
	} table[] = {
		{ "First", 1 },
		{ "Second", 2 },
		{ "Third", 3 },
		{ "Fourth", 4 },
		{ "Last", -1 }
	};
	int i;

	for (i = 0; i < ARRAY_SIZE(table); i++) {
		if (!strcmp(week, table[i].exch))
			return table[i].week;
	}
	g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
		    "Unrecognised DayOfWeekIndex '%s'", week);
	return 0;
}	

gboolean process_deleted_occurrences(icalcomponent *comp, xmlNode *xml_node,
				     icaltimetype **deletia_ret, int *count_ret, GError **error)
{
	icaltimetype *deletia = NULL;
	int count = 0;
	xmlNode *xml_node2;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (strcmp((char *)xml_node->name, "DeletedOccurrence")) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "Unknown element %s in <DeletedOccurrences>",
				    xml_node->name);
			return FALSE;
		}
		for (xml_node2 = xml_node->children; xml_node2; xml_node2 = xml_node2->next) {
			if (xml_node2->type == XML_ELEMENT_NODE &&
			    !strcmp((char *)xml_node2->name, "Start"))
				break;
		}
		if (!xml_node2) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "<DeletedOccurrence> has no <Start>");
			return FALSE;
		}
		count++;
		deletia = realloc(deletia, sizeof(icaltimetype) * count);
		if (!process_time(comp, xml_node2, &deletia[count-1], error)) {
			free(deletia);
			return FALSE;
		}
	}
	*deletia_ret = deletia;
	*count_ret = count;
	return TRUE;
}

gboolean process_modified_occurrences(icalcomponent *comp, xmlNode *xml_node,
				      const char ***modifia_ret, int *count_ret, GError **error)
{
	const gchar **modifia = NULL, *this;
	int count = 0;
	xmlNode *xml_node2;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (strcmp((char *)xml_node->name, "Occurrence")) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "Unknown element %s in <ModifiedOccurrences>",
				    xml_node->name);
			return FALSE;
		}
		for (xml_node2 = xml_node->children; xml_node2; xml_node2 = xml_node2->next) {
			if (xml_node2->type == XML_ELEMENT_NODE &&
			    !strcmp((char *)xml_node2->name, "ItemId"))
				break;
		}
		if (!xml_node2) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "Modified <Occurrence> has no <ItemId>");
			return FALSE;
		}
		this = (char *)xmlGetProp(xml_node2, (xmlChar *)"Id");
		if (!this) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "Modified occurence <ItemId> has no Id= property");
			return FALSE;
		}

		count++;
		modifia = realloc(modifia, sizeof(char *) * count);

		modifia[count-1] = this;
	}
	*modifia_ret = modifia;
	*count_ret = count;
	return TRUE;
}

gboolean process_recurrence(icalcomponent *comp, xmlNode *xml_node, icaltimezone *zone, GError **error)
{
	struct icalrecurrencetype ical_recur;
	char *end_date = NULL, *nr_occurrences = NULL;
	icalproperty *prop;
	xmlNode *xml_node2;

	icalrecurrencetype_clear (&ical_recur);

	if (!zone)
		g_warning("Recurrence with no recognised TimeZone. Hope this is an all-day event");
		
	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "WeeklyRecurrence")) {
			if (!process_weeklyrecurrence(xml_node, &ical_recur, error))
				return FALSE;
		} else if (!strcmp((char *)xml_node->name, "DailyRecurrence")) {
			if (!process_dailyrecurrence(xml_node, &ical_recur, error))
				return FALSE;
		} else if (!strcmp((char *)xml_node->name, "AbsoluteYearlyRecurrence")) {
			if (!process_absoluteyearlyrecurrence(xml_node, &ical_recur, error))
				return FALSE;
		} else if (!strcmp((char *)xml_node->name, "RelativeYearlyRecurrence")) {
			if (!process_relativeyearlyrecurrence(xml_node, &ical_recur, error))
				return FALSE;
		} else if (!strcmp((char *)xml_node->name, "AbsoluteMonthlyRecurrence")) {
			if (!process_absolutemonthlyrecurrence(xml_node, &ical_recur, error))
				return FALSE;
		} else if (!strcmp((char *)xml_node->name, "RelativeMonthlyRecurrence")) {
			if (!process_relativemonthlyrecurrence(xml_node, &ical_recur, error))
				return FALSE;
		} else if (!strcmp((char *)xml_node->name, "EndDateRecurrence")) {
			for (xml_node2 = xml_node->children; xml_node2;
			     xml_node2 = xml_node2->next) {
				if (xml_node2->type != XML_ELEMENT_NODE)
					continue;
				if (!strcmp((char *)xml_node2->name, "EndDate"))
					end_date = (char *)xmlNodeGetContent(xml_node2);
			}
		} else if (!strcmp((char *)xml_node->name, "NumberedRecurrence")) {
			for (xml_node2 = xml_node->children; xml_node2;
			     xml_node2 = xml_node2->next) {
				if (xml_node2->type != XML_ELEMENT_NODE)
					continue;
				if (!strcmp((char *)xml_node2->name, "NumberOfOccurrences")) 
					nr_occurrences = (char *)xmlNodeGetContent(xml_node2);
			}
		}
	}
	if (ical_recur.freq == ICAL_NO_RECURRENCE) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "No recognised Recurrence type");
		return FALSE;
	}

	if (end_date) {
		if (strlen(end_date) != 11 || end_date[4] != '-' ||
		    end_date[7] != '-' || end_date[10] != 'Z') {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "Failed to parse Recurrence EndDate '%s'",
				    end_date);
			return FALSE;
		}
		end_date = strdup(end_date);
		end_date[10] = 0;
		ical_recur.until = icaltime_from_string(end_date);
	} else if (nr_occurrences) {
		ical_recur.count = strtol(nr_occurrences, NULL, 10);
	}
	prop = icalproperty_new_rrule(ical_recur);
	icalcomponent_add_property(comp, prop);
	return TRUE;
}

gboolean process_itemid(icalcomponent *comp, xmlNode *xml_node, GError **error)
{
	const char *id = (char *)xmlGetProp(xml_node, (unsigned char *)"Id");
	if (!id) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<%s> node is empty", xml_node->name);
		return FALSE;
	}

	icalcomponent_set_uid(comp, id);
	return TRUE;
}

gboolean process_reminder_mins(icalcomponent *calcomp, xmlNode *xml_node, GError **error)
{
	const char *minutes;
	int minutesnr;
	icalcomponent *comp;
	icalproperty *prop;
	struct icaltriggertype trig;

	minutes = (char *)xmlNodeGetContent(xml_node);
	if (!minutes) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<%s> node is empty", xml_node->name);
		return FALSE;
	}

	minutesnr = strtol(minutes, NULL, 10);
	
	comp = icalcomponent_new_valarm();
	prop = icalproperty_new_action(ICAL_ACTION_DISPLAY);
	icalcomponent_add_property(comp, prop);
	prop = icalproperty_new_description("REMINDER");
	icalcomponent_add_property(comp, prop);
	trig = icaltriggertype_from_int(-minutesnr * 60);
	prop = icalproperty_new_trigger(trig);
	icalcomponent_add_property(comp, prop);

	icalcomponent_add_component(calcomp, comp);
	return TRUE;
}

static const char *ews_tz_to_ical(const char *ewstz)
{
	static struct {
		const char *exch;
		const char *ical;
	} table[] = {
		/* List found at http://forums.asp.net/p/1518462/3641104.aspx */
		{ "(UTC) Casablanca", "Africa/Casablanca" },
		{ "(UTC) Coordinated Universal Time", "UTC" },
		{ "(UTC) Dublin, Edinburgh, Lisbon, London", "Europe/London" },
		{ "(UTC) Monrovia, Reykjavik", "Atlantic/Reykjavik" },
		{ "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna", "Europe/Amsterdam" },
		{ "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague", "Europe/Belgrade" },
		{ "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris", "Europe/Brussels" },
		{ "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb", "Europe/Sarajevo" },
		{ "(UTC+01:00) West Central Africa", "Africa/Douala" },
		{ "(UTC+02:00) Amman", "Asia/Amman" },
		{ "(UTC+02:00) Athens, Bucharest, Istanbul", "Europe/Athens" },
		{ "(UTC+02:00) Beirut", "Asia/Beirut" },
		{ "(UTC+02:00) Cairo", "Africa/Cairo" },
		{ "(UTC+02:00) Harare, Pretoria", "Africa/Harare" },
		{ "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius", "Europe/Helsinki" },
		{ "(UTC+02:00) Jerusalem", "Asia/Jerusalem" },
		{ "(UTC+02:00) Minsk", "Europe/Minsk" },
		{ "(UTC+02:00) Windhoek", "Africa/Windhoek" },
		{ "(UTC+03:00) Baghdad", "Asia/Baghdad" },
		{ "(UTC+03:00) Kuwait, Riyadh", "Asia/Kuwait" },
		{ "(UTC+03:00) Moscow, St. Petersburg, Volgograd", "Europe/Moscow" },
		{ "(UTC+03:00) Nairobi", "Africa/Nairobi" },
		{ "(UTC+03:30) Tehran", "Asia/Tehran" },
		{ "(UTC+04:00) Abu Dhabi, Muscat", "Asia/Muscat" },
		{ "(UTC+04:00) Baku", "Asia/Baku" },
		{ "(UTC+04:00) Port Louis", "Indian/Mauritius" },
		{ "(UTC+04:00) Tbilisi", "Asia/Tbilisi" },
		{ "(UTC+04:00) Yerevan", "Asia/Yerevan" },
		{ "(UTC+04:30) Kabul", "Asia/Kabul" },
		{ "(UTC+05:00) Ekaterinburg", "Asia/Yekaterinburg" },
		{ "(UTC+05:00) Islamabad, Karachi", "Asia/Karachi" },
		{ "(UTC+05:00) Tashkent", "Asia/Tashkent" },
		{ "(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi", "Asia/Kolkata" },
		{ "(UTC+05:30) Sri Jayawardenepura", "Asia/Colombo" },
		{ "(UTC+05:45) Kathmandu", "Asia/Katmandu" },
		{ "(UTC+06:00) Astana, Dhaka", "Asia/Dhaka" },
		{ "(UTC+06:00) Novosibirsk", "Asia/Novosibirsk" },
		{ "(UTC+06:30) Yangon (Rangoon)", "Asia/Rangoon" },
		{ "(UTC+07:00) Bangkok, Hanoi, Jakarta", "Asia/Bangkok" },
		{ "(UTC+07:00) Krasnoyarsk", "Asia/Krasnoyarsk" },
		{ "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi", "Asia/Shanghai" },
		{ "(UTC+08:00) Irkutsk", "Asia/Irkutsk" },
		{ "(UTC+08:00) Kuala Lumpur, Singapore", "Asia/Kuala_Lumpur" },
		{ "(UTC+08:00) Perth", "Australia/Perth" },
		{ "(UTC+08:00) Taipei", "Asia/Taipei" },
		{ "(UTC+08:00) Ulaanbaatar", "Asia/Ulaanbaatar" },
		{ "(UTC+09:00) Osaka, Sapporo, Tokyo", "Asia/Tokyo" },
		{ "(UTC+09:00) Seoul", "Asia/Seoul" },
		{ "(UTC+09:00) Yakutsk", "Asia/Yakutsk" },
		{ "(UTC+09:30) Adelaide", "Australia/Adelaide" },
		{ "(UTC+09:30) Darwin", "Australia/Darwin" },
		{ "(UTC+10:00) Brisbane", "Australia/Brisbane" },
		{ "(UTC+10:00) Canberra, Melbourne, Sydney", "Australia/Melbourne" },
		{ "(UTC+10:00) Guam, Port Moresby", "Pacific/Guam" },
		{ "(UTC+10:00) Hobart", "Australia/Hobart" },
		{ "(UTC+10:00) Vladivostok", "Asia/Vladivostok" },
		{ "(UTC+11:00) Magadan, Solomon Is., New Caledonia", "Asia/Magadan" },
		{ "(UTC+12:00) Auckland, Wellington", "Pacific/Auckland" },
		{ "(UTC+12:00) Fiji, Marshall Is.", "Pacific/Fiji" },
		{ "(UTC+12:00) Petropavlovsk-Kamchatsky", "Asia/Kamchatka" },
		{ "(UTC+13:00) Nuku'alofa", "Pacific/Tongatapu" },
		{ "(UTC-01:00) Azores", "Atlantic/Azores" },
		{ "(UTC-01:00) Cape Verde Is.", "Atlantic/Cape_Verde" },
		{ "(UTC-02:00) Mid-Atlantic", "Atlantic/South_Georgia" },
		{ "(UTC-03:00) Brasilia", "America/Sao_Paulo" },
		{ "(UTC-03:00) Buenos Aires", "America/Argentina/Buenos_Aires" },
		{ "(UTC-03:00) Cayenne", "America/Cayenne" },
		{ "(UTC-03:00) Greenland", "America/Godthab" },
		{ "(UTC-03:00) Montevideo", "America/Montevideo" },
		{ "(UTC-03:30) Newfoundland", "America/St_Johns" },
		{ "(UTC-04:00) Asuncion", "America/Asuncion" },
		{ "(UTC-04:00) Atlantic Time (Canada)", "America/Halifax" },
		{ "(UTC-04:00) Georgetown, La Paz, San Juan", "America/Argentina/San_Juan" },
		{ "(UTC-04:00) Manaus", "America/Manaus" },
		{ "(UTC-04:00) Santiago", "America/Santiago" },
		{ "(UTC-04:30) Caracas", "America/Caracas" },
		{ "(UTC-05:00) Bogota, Lima, Quito", "America/Bogota" },
		{ "(UTC-05:00) Eastern Time (US & Canada)", "America/New_York" },
		{ "(UTC-05:00) Indiana (East)", "America/Indiana/Indianapolis" },
		{ "(UTC-06:00) Central America", "America/Costa_Rica" },
		{ "(UTC-06:00) Central Time (US & Canada)", "America/Chicago" },
	/* ? */	{ "(UTC-06:00) Guadalajara, Mexico City, Monterrey", "America/Mexico_City" },
		{ "(UTC-06:00) Saskatchewan", "Canada/Saskatchewan" },
		{ "(UTC-07:00) Arizona", "America/Phoenix" },
		{ "(UTC-07:00) Chihuahua, La Paz, Mazatlan", "America/Chihuahua" },
		{ "(UTC-07:00) Mountain Time (US & Canada)", "America/Denver" },
		{ "(UTC-08:00) Pacific Time (US & Canada)", "America/Los_Angeles" },
		{ "(UTC-08:00) Tijuana, Baja California", "America/Tijuana" },
		{ "(UTC-09:00) Alaska", "America/Anchorage" },
		{ "(UTC-10:00) Hawaii", "Pacific/Honolulu" },
		{ "(UTC-11:00) Midway Island, Samoa", "Pacific/Midway" },
	/* ? */	{ "(UTC-12:00) International Date Line West", "Pacific/Apia" },

		/* Extra zones I've seen in testing. Is there *no* sanity in
		 *any* part of what Microsoft does with time zones? */
		{ "(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London", "Europe/London" },
		{ "Pacific Standard Time", "America/Los_Angeles" },
	};
	int i;
	int offset = 0;

	if (!ewstz)
		return NULL;

	/* Sometimes it says 'UTC'; sometimes 'GMT' */
	if (!strncmp(ewstz, "(GMT", 4))
		offset = 4;

	for (i = 0; i < ARRAY_SIZE(table); i++) {
		if (!strcmp(ewstz + offset, table[i].exch + offset))
			return table[i].ical;
	}

	g_warning("Unrecognised TimeZone '%s'", ewstz);
	return NULL;
	
}

icaltimezone *get_timezone(xmlNode *xml_node, GError **error)
{
	icaltimezone *zone = NULL;
	const char *ews_tzname = NULL;
	const char *tzname = NULL;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		else if (!strcmp((char *)xml_node->name, "TimeZone"))
			break;
	}
	if (!xml_node) {
		g_warning("Failed to find TimeZone element; falling back to UTC");
		return NULL;
	}

	ews_tzname = (char *)xmlNodeGetContent(xml_node);

	/* FIXME: Look for manual timezone definitions in the XML and compare against
	   those first, before using the standard Windows ones */

	tzname = ews_tz_to_ical(ews_tzname);
	if (!tzname)
		return NULL;

	zone = icaltimezone_get_builtin_timezone(tzname);
	if (zone)
		return icaltimezone_copy(zone);

	g_warning("Failed to load ical timezone for '%s' (%s)",
		  tzname, ews_tzname);
	return NULL;
}

gboolean get_baseoffset(xmlNode *xml_node, int *retval, GError **error)
{
	const char *baseoffset;
	struct icaldurationtype ofs;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		else if (!strcmp((char *)xml_node->name, "BaseOffset"))
			break;
	}
	if (!xml_node) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<MeetingTimeZone> has no <BaseOffset>");
		return FALSE;
	}
	baseoffset = (const char *)xmlNodeGetContent(xml_node);
	if (!baseoffset) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "<BaseOffset> is empty");
		return FALSE;
	}
	ofs = icaldurationtype_from_string(baseoffset);
	if (icaldurationtype_is_bad_duration(ofs)) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to parse <BaseOffset> '%s'", baseoffset);
		return FALSE;
	}
	if (ofs.is_neg)
		*retval = ofs.minutes * 60;
	else
		*retval = -ofs.minutes * 60;
	return TRUE;
}

gboolean process_relativeyearlyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error)
{
	const char *week = NULL, *month = NULL, *weekday = NULL;
	int weeknr, monthnr, daynr;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "DaysOfWeek")) {
			weekday = (char *)xmlNodeGetContent(xml_node);
		} else if (!strcmp((char *)xml_node->name, "Month")) {
			month = (char *)xmlNodeGetContent(xml_node);
		} else if (!strcmp((char *)xml_node->name, "DayOfWeekIndex")) {
			week = (char *)xmlNodeGetContent(xml_node);
		}
	}
	if (!week || !month || !weekday) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "RelativeYearlyRecurrence missing essential fields (%s,%s,%s)",
			    week, month, weekday);
		return FALSE;
	}
	monthnr = month_to_number(month, error);
	if (!month)
		return FALSE;

	weeknr = weekindex_to_ical(week, error);
	if (!weeknr)
		return FALSE;

	daynr = weekday_to_number(weekday, 10, error);
	if (!daynr)
		return FALSE;

	icalrecurrencetype_clear(ical_recur);
	ical_recur->freq = ICAL_YEARLY_RECURRENCE;
	ical_recur->by_month[0] = monthnr;


	if (daynr < 8) {
		if (weeknr > 0)
			ical_recur->by_day[0] = daynr + (weeknr * 8);
		else
			ical_recur->by_day[0] = -8 - daynr;
	} else if (daynr == 8) { /* Day */
		ical_recur->by_month_day[0] = weeknr;
	} else if (daynr == 9) { /* Weekday */
		ical_recur->by_day[0] = 2;
		ical_recur->by_day[1] = 3;
		ical_recur->by_day[2] = 4;
		ical_recur->by_day[3] = 5;
		ical_recur->by_day[4] = 6;
		ical_recur->by_set_pos[0] = weeknr;
	} else if (daynr == 10) { /* WeekendDay */
		ical_recur->by_day[0] = 1;
		ical_recur->by_day[1] = 7;
		ical_recur->by_set_pos[0] = weeknr;
	}		

	return TRUE;
}

gboolean process_absoluteyearlyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error)
{
	const char *day_of_month = NULL;
	const char *month = NULL;
	int daynr, monthnr;

	for (xml_node = xml_node->children; xml_node;
	     xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "DayOfMonth"))
			day_of_month = (char *)xmlNodeGetContent(xml_node);
		else if (!strcmp((char *)xml_node->name, "Month"))
			month = (char *)xmlNodeGetContent(xml_node);
	}
	if (!day_of_month || !month) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "AbsoluteYearlyRecurrence missing essential fields (%s,%s)",
			    day_of_month, month);
		return FALSE;
	}
	daynr = strtol(day_of_month, NULL, 10);
	if (!daynr) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to parse DayOfMonth '%s'", day_of_month);
		return FALSE;
	}
	monthnr = month_to_number(month, error);
	if (!month)
		return FALSE;

	icalrecurrencetype_clear(ical_recur);
	ical_recur->freq = ICAL_YEARLY_RECURRENCE;
	ical_recur->by_month[0] = monthnr;
	ical_recur->by_month_day[0] = daynr;
	return TRUE;
}

gboolean process_relativemonthlyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error)
{
	const char *week = NULL, *interval = NULL, *weekday = NULL;
	int weeknr, intervalnr, daynr;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "DaysOfWeek")) {
			weekday = (char *)xmlNodeGetContent(xml_node);
		} else if (!strcmp((char *)xml_node->name, "Interval")) {
			interval = (char *)xmlNodeGetContent(xml_node);
		} else if (!strcmp((char *)xml_node->name, "DayOfWeekIndex")) {
			week = (char *)xmlNodeGetContent(xml_node);
		}
	}
	if (!week || !interval || !weekday) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "RelativeMonthlyRecurrence missing essential fields (%s,%s,%s)",
			    week, interval, weekday);
		return FALSE;
	}
	intervalnr = strtol(interval, NULL, 10);
	if (!intervalnr) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to parse Interval '%s'", interval);
		return FALSE;
	}
	weeknr = weekindex_to_ical(week, error);
	if (!weeknr)
		return FALSE;

	daynr = weekday_to_number(weekday, 10, error);
	if (!daynr)
		return FALSE;

	icalrecurrencetype_clear(ical_recur);
	ical_recur->freq = ICAL_MONTHLY_RECURRENCE;
	ical_recur->interval = intervalnr;
	if (daynr < 8) {
		if (weeknr > 0)
			ical_recur->by_day[0] = daynr + (weeknr * 8);
		else
			ical_recur->by_day[0] = -8 - daynr;
	} else if (daynr == 8) { /* Day */
		ical_recur->by_month_day[0] = weeknr;
	} else if (daynr == 9) { /* Weekday */
		ical_recur->by_day[0] = 2;
		ical_recur->by_day[1] = 3;
		ical_recur->by_day[2] = 4;
		ical_recur->by_day[3] = 5;
		ical_recur->by_day[4] = 6;
		ical_recur->by_set_pos[0] = weeknr;
	} else if (daynr == 10) { /* WeekendDay */
		ical_recur->by_day[0] = 1;
		ical_recur->by_day[1] = 7;
		ical_recur->by_set_pos[0] = weeknr;
	}
	return TRUE;
}

gboolean process_absolutemonthlyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur,
					   GError **error)
{
	const char *interval = NULL, *monthday = NULL;
	int intervalnr, monthdaynr;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "DayOfMonth")) {
			monthday = (char *)xmlNodeGetContent(xml_node);
		} else if (!strcmp((char *)xml_node->name, "Interval")) {
			interval = (char *)xmlNodeGetContent(xml_node);
		}
	}
	if (!interval || !monthday) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "AbsoluteMonthlyRecurrence missing essential fields (%s,%s)",
			    interval, monthday);
		return FALSE;
	}
	intervalnr = strtol(interval, NULL, 10);
	if (!intervalnr) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to parse Interval '%s'", interval);
		return FALSE;
	}
	monthdaynr = strtol(monthday, NULL, 10);
	if (!monthday) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to parse DayOfMonth '%s'", monthday);
		return FALSE;
	}

	icalrecurrencetype_clear(ical_recur);
	ical_recur->freq = ICAL_MONTHLY_RECURRENCE;
	ical_recur->interval = intervalnr;
	ical_recur->by_month_day[0] = monthdaynr;
	return TRUE;
}

gboolean weekdays_to_recur_byday(const char *days, struct icalrecurrencetype *ical_recur, GError **error)
{
	const char *space;
	const char *day;
	int count = 0;
	int daynr;

	do {
		space = strchr(days, ' ');
		if (space)
			day = g_strndup(days, space - days);
		else
			day = days;

		daynr = weekday_to_number(day, 7, error);
		if (!daynr)
			return FALSE;

		if (count == ICAL_BY_DAY_SIZE) {
			g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
				    "Too many days in DaysOfWeek list");
			return FALSE;
		}

		ical_recur->by_day[count++] = daynr;
		if (space) {
			free((char *)day);
			days = space + 1;
		}
	} while (space);
	return TRUE;
}

gboolean process_weeklyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error)
{
	const char *interval = NULL, *weekday = NULL, *firstday = NULL;
	int intervalnr, firstdaynr;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "DaysOfWeek")) {
			weekday = (char *)xmlNodeGetContent(xml_node);
		} else if (!strcmp((char *)xml_node->name, "Interval")) {
			interval = (char *)xmlNodeGetContent(xml_node);
		} else if (!strcmp((char *)xml_node->name, "FirstDayOfWeek")) {
			firstday = (char *)xmlNodeGetContent(xml_node);
		}
	}
	if (!interval || !weekday) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "WeeklyRecurrence missing essential fields (%s,%s)",
			    interval, weekday);
		return FALSE;
	}
	intervalnr = strtol(interval, NULL, 10);
	if (!intervalnr) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to parse Interval '%s'", interval);
		return FALSE;
	}
	if (firstday)
		firstdaynr = weekday_to_number(firstday, 7, error);
	else
		firstdaynr = 0;

	icalrecurrencetype_clear(ical_recur);
	ical_recur->interval = intervalnr;
	ical_recur->week_start = firstdaynr;

	if (!weekdays_to_recur_byday(weekday, ical_recur, error))
		return FALSE;

	ical_recur->freq = ICAL_WEEKLY_RECURRENCE;
	return TRUE;
}

gboolean process_dailyrecurrence(xmlNode *xml_node, struct icalrecurrencetype *ical_recur, GError **error)
{
	const char *interval = NULL;
	int intervalnr;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "Interval")) {
			interval = (char *)xmlNodeGetContent(xml_node);
		}
	}
	if (!interval) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE, 
			    "DailyRecurrence missing essential fields (%s)",
			    interval);
		return FALSE;
	}
	intervalnr = strtol(interval, NULL, 10);
	if (!intervalnr) {
		g_set_error(error, EWS_ERROR, EWS_ERROR_PARSE,
			    "Failed to parse Interval '%s'", interval);
		return FALSE;
	}

	icalrecurrencetype_clear(ical_recur);
	ical_recur->freq = ICAL_DAILY_RECURRENCE;
	ical_recur->interval = intervalnr;
	return TRUE;
}

icalcomponent *process_timezone_rule(xmlNode *xml_node, icalcomponent_kind kind,
				     int *offset, GError **error)
{
	icalcomponent *comp = icalcomponent_new(kind);
	char *tzname;
	icalproperty *prop;

	tzname = (char *)xmlGetProp(xml_node, (xmlChar *)"TimeZoneName");
	if (tzname) {
		prop = icalproperty_new_tzname(tzname);
		icalcomponent_add_property(comp, prop);
	}

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		else if (!strcmp((char *)xml_node->name, "Offset")) {
			char *ofs_string = (char *)xmlNodeGetContent(xml_node);
			struct icaldurationtype ofs = icaldurationtype_from_string(ofs_string);
			if (ofs.is_neg)
				*offset += ofs.minutes * 60;
			else
				*offset -= ofs.minutes * 60;

			prop = icalproperty_new_tzoffsetto(*offset);
			icalcomponent_add_property(comp, prop);
		} else if (!strcmp((char *)xml_node->name, "RelativeYearlyRecurrence")) {
			struct icalrecurrencetype ical_recur;

			if (!process_relativeyearlyrecurrence(xml_node, &ical_recur, error))
				return NULL;
			prop = icalproperty_new_rrule(ical_recur);
			icalcomponent_add_property(comp, prop);
		} else if (!strcmp((char *)xml_node->name, "AbsoluteDate")) {
			/* Are there really timezones which change on the same date
			   every year? */
			g_warning("Don't know how to handle AbsoluteDate for timezone change: '%s'",
				  xmlNodeGetContent(xml_node));
		} else if (!strcmp((char *)xml_node->name, "Time")) {
			struct icaltimetype dtstart;
			char *time_string = (char *)xmlNodeGetContent(xml_node);

			if (strlen(time_string) != 8 || time_string[2] != ':' ||
			    time_string[5] != ':') {
				g_warning("Cannot parse dst change time '%s'",
					  time_string);
				return NULL;
			}
			memset(&dtstart, 0, sizeof(dtstart));
			dtstart.year = 1900;
			dtstart.month = 1;
			dtstart.day = 1;
			dtstart.hour = strtol(time_string, NULL, 10);
			dtstart.minute = strtol(time_string + 3, NULL, 10);
			dtstart.second = strtol(time_string + 6, NULL, 10);

			prop = icalproperty_new_dtstart(dtstart);
			icalcomponent_add_property(comp, prop);
		}
	}
	return comp;
}

icaltimezone *get_meeting_timezone(xmlNode *xml_node, GError **error)
{
	icalcomponent *comp = NULL, *dst_zone = NULL, *std_zone = NULL;
	icalproperty *prop;
	icaltimezone *z;
	const char *tzname = NULL;

	int std_offset, dst_offset;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		else if (!strcmp((char *)xml_node->name, "MeetingTimeZone"))
			break;
	}
	if (!xml_node)
		return NULL;
	comp = icalcomponent_new(ICAL_VTIMEZONE_COMPONENT);
	
	tzname = (const char *)xmlGetProp(xml_node, (xmlChar *)"TimeZoneName");

	prop = icalproperty_new_tzid(tzname);
	icalcomponent_add_property(comp, prop);

	if (!get_baseoffset(xml_node, &std_offset, error))
		return NULL;
	dst_offset = std_offset;

	for (xml_node = xml_node->children; xml_node; xml_node = xml_node->next) {
		if (xml_node->type != XML_ELEMENT_NODE)
			continue;
		if (!strcmp((char *)xml_node->name, "BaseOffset"))
			continue;
		else if (!strcmp((char *)xml_node->name, "Standard")) {
			std_zone = process_timezone_rule(xml_node,
							 ICAL_XSTANDARD_COMPONENT,
							 &std_offset, error);
		} else if (!strcmp((char *)xml_node->name, "Daylight")) {
			dst_zone = process_timezone_rule(xml_node,
							 ICAL_XDAYLIGHT_COMPONENT,
							 &dst_offset, error);
		} else {
			g_warning("Unknown element in MeetingTimeZone: %s",
				  xml_node->name);
		}
	}

	if (std_zone && dst_zone) {
		prop = icalproperty_new_tzoffsetfrom(dst_offset);
		icalcomponent_add_property(std_zone, prop);

		prop = icalproperty_new_tzoffsetfrom(std_offset);
		icalcomponent_add_property(dst_zone, prop);

		icalcomponent_add_component(comp, std_zone);
		icalcomponent_add_component(comp, dst_zone);
	} else {
		if (!std_zone) {
			std_zone = icalcomponent_new(ICAL_XSTANDARD_COMPONENT);

			prop = icalproperty_new_tzoffsetto(std_offset);
			icalcomponent_add_property(std_zone, prop);

			prop = icalproperty_new_tzoffsetfrom(std_offset);
			icalcomponent_add_property(std_zone, prop);
		}
		icalcomponent_add_component(comp, std_zone);
	}		

	z = icaltimezone_new();
	icaltimezone_set_component(z, comp);
	return z;
}
