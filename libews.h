/*
 * libews.h -- Exchange Web Services
 *
 * Copyright © 2010 Intel Corporation
 *
 * Author: David Woodhouse <dwmw2@infradead.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __LIBEWS_H__
#define __LIBEWS_H__

#include <libical/icalcomponent.h>
#include <glib/gerror.h>
#include <glib/gquark.h>

icalcomponent *ews_calitem_to_ical(xmlNode *xml_node, const gchar *parent_id,
				   icaltimezone *parent_zone, 
				   icalcomponent *(fetch_subitem)(void *priv,
								  const gchar *item_id,
								  const gchar *parent_id,
								  icaltimezone *zone,
								  GError **error),
				   void *fetch_subitem_priv, GError **error);

GQuark ews_error_quark();

#define EWS_ERROR (ews_error_quark())

typedef enum {
	EWS_ERROR_PARSE,
	EWS_ERROR_ROUTING_UNKNOWN,
	EWS_ERROR_ROUTING_EX,
	EWS_ERROR_SOAP,
	EWS_ERROR_SERVER,
} EwsError;

#endif
