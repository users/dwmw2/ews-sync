#!/bin/sh

MYDIR="$(dirname $0)"
CURLAUTH="--negotiate -u dummy:"
# NTLM is faster, but requires your password

if [ "$3" != "" ]; then
    CURLAUTH="--ntlm -u $2:$3"
fi
CURLAUTH="-k $CURLAUTH"
NOPROXY=$(echo $1 | cut -f3 -d/)
CURLAUTH="--noproxy $NOPROXY $CURLAUTH"

EWSURL="$1"

mkdir -p ~/ews-sync/ical || exit 1

OLDSYNCSTATE=$(cat ~/.ews-syncstate)

QUERYFILE=$(mktemp /tmp/ewsqueryXXXXXX)
RESULTFILE=$(mktemp /tmp/ewsresultXXXXXX)
CHANGELIST=$(mktemp /tmp/ewsclistXXXXXX)
trap 'rm $QUERYFILE $RESULTFILE $CHANGELIST' EXIT

cat > $QUERYFILE <<EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
  xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types">
  <soap:Body>
    <SyncFolderItems xmlns="http://schemas.microsoft.com/exchange/services/2006/messages">
      <ItemShape>
        <t:BaseShape>IdOnly</t:BaseShape>
      </ItemShape>
      <SyncFolderId>
        <t:DistinguishedFolderId Id="calendar"/>
      </SyncFolderId>
EOF
if [ "$OLDSYNCSTATE" != "" ]; then
    echo "<SyncState>$OLDSYNCSTATE</SyncState>" >> $QUERYFILE
fi
cat >> $QUERYFILE <<EOF
      <MaxChangesReturned>50</MaxChangesReturned>
    </SyncFolderItems>
  </soap:Body>
</soap:Envelope>
EOF

MORETOCOME=false

if ! curl $CURLAUTH -L -H "Content-Type: text/xml" \
    $EWSURL -d @$QUERYFILE > $RESULTFILE; then
    exit 1
fi

echo
# I'm far too lazy to do proper XML parsing for a proof-of-concept
(sed 's/\(<[^/]\)/\n\1/g' $RESULTFILE; echo) | while read LINE; do
    case $LINE in
	"<t:Create>"*)
	    TYPE=CREATE
	    ;;
	"<t:Update>"*)
	    TYPE=UPDATE
	    ;;
	"<t:Delete>"*)
	    TYPE=DELETE
	    ;;
	"<t:ItemId "*)
	    ITEMID=$(echo $LINE | sed 's/.* Id="\([^"]*\)" .*/\1/')
	    ITEMCHANGEKEY=$(echo $LINE | sed 's/.* ChangeKey="\([^"]*\)" .*/\1/')

	    echo $TYPE $ITEMID $ITEMCHANGEKEY >> $CHANGELIST
	    ;;
	*)
	    ;;
    esac
done

if grep -q '<m:IncludesLastItemInRange>false</m:I' $RESULTFILE ; then
    MORETOCOME=true
fi

NEWSYNCSTATE=$(sed -n '/<m:SyncState>/{s/.*<m:SyncState>\(.*\)<\/m:SyncState>.*/\1/p}' $RESULTFILE)

if [ "$NEWSYNCSTATE" = "" ]; then
    sed 's/\(<[^/]\)/\n\1/g' $RESULTFILE
    echo
    echo
    echo "No <SyncState> found in response (shown above); aborting"
    exit 1
fi

cat $CHANGELIST | while read TYPE ITEMID ITEMCHANGEKEY; do
    ITEMFILENAME=~/ews-sync/$(echo $ITEMID | sed s^/^%2f^g).xml
    ICALFILENAME=~/ews-sync/ical/$(echo $ITEMID | sed s^/^%2f^g).ics
    echo $TYPE $ITEMID
    case $TYPE in
	DELETE)
	    rm "$ITEMFILENAME"
	    rm "$ICALFILENAME"
	    ;;
	UPDATE|CREATE)
	    cat > $QUERYFILE <<EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
  xmlns:t="http://schemas.microsoft.com/exchange/services/2006/types">
  <soap:Body>
    <GetItem xmlns="http://schemas.microsoft.com/exchange/services/2006/messages">
      <ItemShape>
        <t:BaseShape>AllProperties</t:BaseShape>
        <t:BodyType>Text</t:BodyType>
        <t:AdditionalProperties>
          <t:FieldURI FieldURI="item:ResponseObjects"/>
        </t:AdditionalProperties>
      </ItemShape>
      <ItemIds>
<t:ItemId Id="$ITEMID" ChangeKey="$ITEMCHANGEKEY"/>
      </ItemIds>
    </GetItem>
  </soap:Body>
</soap:Envelope>
EOF
	    if ! curl $CURLAUTH -L -H "Content-Type: text/xml" \
		$EWSURL -d @$QUERYFILE > $RESULTFILE; then
		exit 1
	    fi
	    sed 's/\(<[^/]\)/\n\1/g' $RESULTFILE > "$ITEMFILENAME"
	    ${MYDIR}/ews2ical "$ITEMFILENAME" "$ICALFILENAME"
	    ;;
    esac
done
echo $NEWSYNCSTATE > ~/.ews-syncstate
if [ "$MORETOCOME" == "true" ]; then
    echo Restarting to fetch more items
    exec "$0" "$@"
fi
( cat <<EOF
BEGIN:VCALENDAR
METHOD:PUBLISH
PRODID:ews-sync
VERSION:2.0
EOF
 egrep -hv '^BEGIN:VCALENDAR|METHOD:PUBLISH|VERSION:2.0|END:VCALENDAR' ~/ews-sync/ical/*.ics
 echo 'END:VCALENDAR'
) > ~/ews-sync/all.ics
